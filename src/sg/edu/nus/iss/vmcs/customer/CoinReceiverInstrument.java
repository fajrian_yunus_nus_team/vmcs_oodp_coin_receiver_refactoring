package sg.edu.nus.iss.vmcs.customer;

public interface CoinReceiverInstrument {

	public void startReceiver();
	public void onInvalidCoinReceived();
	public void onValidCoinReceived(int totalInserted);
	public void continueReceive();
	public void stopReceive();
	public void refundCash(int totalInserted);
	public void setActive(boolean active);
	
	
}
